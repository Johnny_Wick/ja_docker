# ja_docker

- docker example
    - docker move local folder to contianer's path

---
![img](img/docker_mv.jpg)


---

- ja_docker_run_simple_web ✅
    - docker run -it --rm ${pwd}:/moving_from_host busybox sh
    - from 32768 : 80
    - curl --request GET http://localhost:32768 
     
--- 

![result](img/nginx.jpg) 



---

  ## Docker version:
---

- version 1: need --link to different instance
- version 2: Service -> no need for link
- version 3: Service -> new feature (docker swarm... etc.. later 🦜...)

--- 
![img](img/docker_version.jpg)
--- 



## Docker network simple structure demo
---

![simple](img/docker_network_be_fe.jpg )

---

## Docker hub images notation
---
- docker.io / account id + image repository 
- nginx: 
    - nginx / nginx

---
## Private Registry 

---
- create your own private internal docker-image-repo enpoint
- docker push localhost:5000/my_docker_image_repo
- docker pull 192.168.56.100:5000/my_docker_image_repo

---

![img](img/register_ur_own_docker_repo.jpg)

---

---

---

![pic](img/docker_image_hub_notation.jpg)

---


- docker shell

