# Kafka  🦜

---
-  __main__ 
-  referenced. [#ref]






--- 




<a name="ref" ></a>
--- 

## Run Kafka via Docker images

---
- use docker-compose.yml
- setting the port 
- it's good to speed up the deploy.
- ![link](https://better-coding.com/building-apache-kafka-cluster-using-docker-compose-and-virtualbox/)

---

## Golang Micro-services by saving Data to Mongodb via Kafka

---

- Go example to saving data in this way.
- ![Link](https://www.melvinvivas.com/developing-microservices-using-kafka-and-mongodb/)
- Deraft Structure
    - client send rest request user-services (ignore gate and auth structure , focus on data-steaming demo) 😈
    - gate-services (build-in producer) , send content to topic "new Register Request"
    - user-services (build-in consumer ) recieved the register request in topic "register Request queue" and start to saving data to mongo one-by-one
    - then, user-services , send the topic "job-list with this job-id" for done , or to topic "push-broad-cast" to user (skip)
    - Or, gate-services via grpc method (check user_exist (by_id) ) to check. (skip)



